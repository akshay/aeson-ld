{-# LANGUAGE OverloadedStrings #-}

module Data.AesonLD.Term (Term, termFromText, termToText, termAppendText) where

import Control.Monad (when)
import Data.Aeson
import Data.Text (Text)
import qualified Data.Text as Text
import Data.AesonLD.Keyword (isKeywordForm)

newtype Term = Term Text
  deriving (Show, Eq, Ord)

termFromText :: MonadFail m => Text -> m Term
termFromText t = do
  when (isKeywordForm t) $
    fail ("A term must not look like a keyword, got: " <> show t)

  when (Text.null t) $
    fail "A term must not be an empty string"
  pure $ Term t

termToText :: Term -> Text
termToText (Term t) = t

termAppendText :: Term -> Text -> Term
termAppendText (Term t1) t2 = Term $ t1 <> t2

instance FromJSON Term where
  parseJSON = withText "Term" termFromText
