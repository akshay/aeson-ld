{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.AesonLD.Keyword
  ( Keyword (..),
    parseKeyword,
    keywords,
    isKeywordForm,
    Reserved,
    parseReserved,
    reservedText,
    parseKeywordOrReserved,
  )
where

import Data.Aeson
import qualified Data.Char as Char
import Data.Text (Text)
import qualified Data.Text as Text

data Keyword
  = KeywordBase
  | KeywordContainer
  | KeywordContext
  | KeywordDirection
  | KeywordGraph
  | KeywordId
  | KeywordImport
  | KeywordIncluded
  | KeywordIndex
  | KeywordJson
  | KeywordLanguage
  | KeywordList
  | KeywordNest
  | KeywordNone
  | KeywordPrefix
  | KeywordPropagate
  | KeywordProtected
  | KeywordReverse
  | KeywordSet
  | KeywordType
  | KeywordValue
  | KeywordVersion
  | KeywordVocab
  deriving (Show, Eq, Ord)

instance FromJSON Keyword where
  parseJSON = withText "Keyword" parseKeyword

instance ToJSON Keyword where
  toJSON = \case
    KeywordBase -> String "@base"
    KeywordContainer -> String "@container"
    KeywordContext -> String "@context"
    KeywordDirection -> String "@direction"
    KeywordGraph -> String "@graph"
    KeywordId -> String "@id"
    KeywordImport -> String "@import"
    KeywordIncluded -> String "@included"
    KeywordIndex -> String "@index"
    KeywordJson -> String "@json"
    KeywordLanguage -> String "@language"
    KeywordList -> String "@list"
    KeywordNest -> String "@nest"
    KeywordNone -> String "@none"
    KeywordPrefix -> String "@prefix"
    KeywordPropagate -> String "@propagate"
    KeywordProtected -> String "@protected"
    KeywordReverse -> String "@reverse"
    KeywordSet -> String "@set"
    KeywordType -> String "@type"
    KeywordValue -> String "@value"
    KeywordVersion -> String "@version"
    KeywordVocab -> String "@vocab"

parseKeyword :: MonadFail m => Text -> m Keyword
parseKeyword t = do
  rOrK <- parseKeywordOrReserved Right Left t
  case rOrK of
    Right k -> pure k
    Left _ -> fail $ "Invalid keyword: " <> show t

parseKeywordOrReserved :: MonadFail m => (Keyword -> a) -> (Reserved -> a) -> Text -> m a
parseKeywordOrReserved keyword reserved = \case
  "@base" -> pure (keyword KeywordBase)
  "@container" -> pure (keyword KeywordContainer)
  "@context" -> pure (keyword KeywordContext)
  "@direction" -> pure (keyword KeywordDirection)
  "@graph" -> pure (keyword KeywordGraph)
  "@id" -> pure (keyword KeywordId)
  "@import" -> pure (keyword KeywordImport)
  "@included" -> pure (keyword KeywordIncluded)
  "@index" -> pure (keyword KeywordIndex)
  "@json" -> pure (keyword KeywordJson)
  "@language" -> pure (keyword KeywordLanguage)
  "@list" -> pure (keyword KeywordList)
  "@nest" -> pure (keyword KeywordNest)
  "@none" -> pure (keyword KeywordNone)
  "@prefix" -> pure (keyword KeywordPrefix)
  "@propagate" -> pure (keyword KeywordPropagate)
  "@protected" -> pure (keyword KeywordProtected)
  "@reverse" -> pure (keyword KeywordReverse)
  "@set" -> pure (keyword KeywordSet)
  "@type" -> pure (keyword KeywordType)
  "@value" -> pure (keyword KeywordValue)
  "@version" -> pure (keyword KeywordVersion)
  "@vocab" -> pure (keyword KeywordVocab)
  t ->
    if isKeywordForm t
      then pure (reserved $ Reserved t)
      else fail $ "Expected '@' followed by ALPHA characters, got: " <> show t

keywords :: [Text]
keywords =
  [ "@base",
    "@container",
    "@context",
    "@direction",
    "@graph",
    "@id",
    "@import",
    "@included",
    "@index",
    "@json",
    "@language",
    "@list",
    "@nest",
    "@none",
    "@prefix",
    "@propagate",
    "@protected",
    "@reverse",
    "@set",
    "@type",
    "@value",
    "@version",
    "@vocab"
  ]

newtype Reserved = Reserved Text
  deriving (Show, Eq, Ord)

parseReserved :: MonadFail f => Text -> f Reserved
parseReserved t = do
  rOrK <- parseKeywordOrReserved Right Left t
  case rOrK of
    Left r -> pure r
    Right _ -> fail $ "Already a keyword: " <> show t

reservedText :: Reserved -> Text
reservedText (Reserved t) = t

isKeywordForm :: Text -> Bool
isKeywordForm t =
  Text.head t == '@'
    && Text.length t >= 2
    && Text.all Char.isAlpha (Text.tail t)
