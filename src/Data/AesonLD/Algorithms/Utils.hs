{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Data.AesonLD.Algorithms.Utils where

import Data.Maybe (fromMaybe)
import Data.RDF.Types (IRI (..), IRIAuth (..))
import Data.Text (Text)
import qualified Data.Text as Text
import Network.URI (URI (..), URIAuth (..))

iriToURI :: IRI -> URI
iriToURI iri =
  URI
    { uriScheme = Text.unpack (iriScheme iri) <> ":",
      uriAuthority = case iriAuth iri of
        Nothing -> Nothing
        Just auth ->
          Just $
            URIAuth
              { uriUserInfo = maybe "" (Text.unpack . (<> "@")) $ iriUser auth,
                uriRegName = Text.unpack $ iriHost auth,
                uriPort = maybe "" (Text.unpack . (":" <>)) $ iriPort auth
              },
      uriPath = Text.unpack $ iriPath iri,
      uriQuery = maybe "" (Text.unpack . ("?" <>)) $ iriQuery iri,
      uriFragment = maybe "" (Text.unpack . ("#" <>)) $ iriFragment iri
    }

dropSuffix :: Text -> Text -> Text
dropSuffix s t = fromMaybe t $ Text.stripSuffix s t

dropPrefix :: Text -> Text -> Text
dropPrefix s t = fromMaybe t $ Text.stripPrefix s t

emptyToNothing :: [a] -> Maybe [a]
emptyToNothing [] = Nothing
emptyToNothing xs = Just xs

uriToIRI :: URI -> IRI
uriToIRI URI {..} =
  IRI
    { iriScheme = dropSuffix ":" . Text.pack $ uriScheme,
      iriAuth = case uriAuthority of
        Nothing -> Nothing
        Just URIAuth {..} ->
          Just $
            IRIAuth
              { iriUser =
                  if null uriUserInfo
                    then Nothing
                    else Just . dropSuffix "@" $ Text.pack uriUserInfo,
                iriHost = Text.pack uriRegName,
                iriPort = dropPrefix ":" . Text.pack <$> emptyToNothing uriPort
              },
      iriPath = Text.pack uriPath,
      iriQuery = dropPrefix "?" . Text.pack <$> emptyToNothing uriQuery,
      iriFragment = dropPrefix "#" . Text.pack <$> emptyToNothing uriFragment
    }
