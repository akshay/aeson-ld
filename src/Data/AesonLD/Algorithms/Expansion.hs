{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ViewPatterns #-}

module Data.AesonLD.Algorithms.Expansion where

import Control.Applicative ((<|>))
import Control.Exception (Exception, throwIO)
import Control.Monad (foldM, join, unless, when, (<=<))
import Data.Aeson
import qualified Data.Aeson.Key as AesonKey
import qualified Data.Aeson.Key as Key
import qualified Data.Aeson.KeyMap as KM
import Data.AesonLD.Algorithms.ContextProcessing (expandIRIWithActiveContext, newActiveContext, newActiveContextDefaults)
import Data.AesonLD.Algorithms.ContextProcessing.Internal
import Data.AesonLD.Algorithms.Utils
import Data.AesonLD.ContextDefinition
import Data.AesonLD.Keyword
import Data.AesonLD.Term
import Data.Foldable (foldl')
import Data.IORef
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (isJust, isNothing, mapMaybe)
import Data.RDF.Types (IRI)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Vector as V
import Lens.Micro (to, (^.), _Just)
import qualified Network.HTTP.Client as HTTP
import Network.URI (uriToString)

expandValue :: ActiveContext -> Maybe Text -> Value -> Value
expandValue activeCtx activeProperty value =
  let activeTermDefKey = termDefKeyFromText =<< activeProperty
      activeTermDef = flip Map.lookup (acTermDefs activeCtx) =<< activeTermDefKey
      activeTypeMapping = atdTypeMapping =<< activeTermDef
   in case (value, activeTypeMapping) of
        (String s, Just (TypeKeyword TypeId)) ->
          let expanded = case termDefKeyFromText s of
                Nothing -> value
                Just t ->
                  toJSON . fmap expandedIriToJSON $
                    expandIRIWithActiveContext True False activeCtx (Right t)
           in object ["@id" .= expanded]
        (String s, Just (TypeKeyword TypeVocab)) ->
          let expanded = case termDefKeyFromText s of
                Nothing -> value
                Just t ->
                  toJSON . fmap expandedIriToJSON $
                    expandIRIWithActiveContext True True activeCtx (Right t)
           in object ["@id" .= expanded]
        _ ->
          let (language, direction) =
                case value of
                  String _ -> (atdLanguageMapping =<< activeTermDef, atdDirection =<< activeTermDef)
                  _ -> (Nothing, Nothing)
           in object $
                ["@value" .= value]
                  ++ ["@language" .= language | isJust language]
                  ++ ["@direction" .= direction | isJust direction]

expandedIriToJSON :: Either Keyword IRI -> Value
expandedIriToJSON = \case
  (Left k) -> toJSON k
  (Right i) -> toJSON $ uriToString id (iriToURI i) ""

expand :: HTTP.Manager -> ActiveContext -> Maybe Text -> Value -> IRI -> Bool -> Bool -> Bool -> IO Value
expand mgr activeCtx activeProperty element baseUrl frameExpansion ordered fromMap = do
  let frameExpansion' =
        activeProperty /= Just "@default" && frameExpansion
      activeTermDefKey = termDefKeyFromText =<< activeProperty
      activeTermDef = flip Map.lookup (acTermDefs activeCtx) =<< activeTermDefKey
      propertyScopedCtx = atdLocalContext =<< activeTermDef
      expandScalar =
        if activeProperty `elem` [Nothing, Just "@graph"]
          then pure Null
          else do
            activeCtx' <- case propertyScopedCtx of
              Nothing -> pure activeCtx
              Just (termBaseIRI, localCtx) -> newActiveContextDefaults mgr activeCtx localCtx termBaseIRI
            pure $ expandValue activeCtx' activeProperty element
      expandArrayElement v = do
        expandedItem <- expand mgr activeCtx activeProperty v baseUrl frameExpansion' ordered fromMap
        let expandedItem' =
              if (atdContainerMapping =<< activeTermDef) == Just ContainerList
                then object ["@list" .= expandedItem]
                else expandedItem
        case expandedItem' of
          Null -> pure V.empty
          Array xs -> pure xs
          _ -> pure $ V.singleton expandedItem'
  case element of
    Null -> pure Null
    String _ -> expandScalar
    Number _ -> expandScalar
    Bool _ -> expandScalar
    Array vs -> Array <$> foldMapM expandArrayElement vs
    Object o -> do
      let expandText = expandIRIWithActiveContext False True activeCtx <=< termDefKeyOrKeywordFromText
          expandKey = expandText . AesonKey.toText
          expandedKeysMap = Map.fromSet expandKey . Map.keysSet $ KM.toMap o
      activeCtx1 <-
        case acPreviousContext activeCtx of
          Just prevCtx | not fromMap -> do
            if Just (Left KeywordValue) `elem` expandedKeysMap || Map.elems expandedKeysMap == [Just (Left KeywordId)]
              then pure activeCtx
              else pure prevCtx
          _ -> pure activeCtx
      activeCtx2 <- case propertyScopedCtx of
        Nothing -> pure activeCtx1
        Just (termBaseIRI, localCtx) -> newActiveContext ProcessinMode1_1 mgr activeCtx localCtx termBaseIRI [] True True True
      activeCtx3 <-
        case fromJSON <$> KM.lookup "@context" o of
          Nothing -> pure activeCtx2
          Just (Success localCtx) -> newActiveContextDefaults mgr activeCtx localCtx baseUrl
          Just (Error _) ->
            -- TODO: Is this correct?
            throwIO InvalidContextEntry
      let typeScopedCtx = activeCtx3
          typeKeys = Map.keysSet $ Map.filter (== Just (Left KeywordType)) expandedKeysMap
          typeVals = Map.restrictKeys (KM.toMap o) typeKeys
          activeCtxFromTerm ac t =
            case atdLocalContext =<< Map.lookup t (acTermDefs typeScopedCtx) of
              Just (termBaseIRI, localCtx) -> newActiveContext ProcessinMode1_1 mgr ac localCtx termBaseIRI [] False False True
              Nothing -> pure ac
          activeCtxFromTypeVal ac val =
            let termsText = case val of
                  String s -> [s]
                  Array xs ->
                    mapMaybe
                      (resultToMaybe . fromJSON)
                      (V.toList xs)
                  _ -> []
                terms = mapMaybe termDefKeyFromText termsText
             in foldM activeCtxFromTerm ac terms
      activeCtx4 <- foldM activeCtxFromTypeVal activeCtx3 typeVals
      let result0 = Map.empty
          nests0 = Map.empty
          typeKey = Set.lookupMin typeKeys
          inputTypeText =
            case Map.lookupMin typeVals of
              Just (_, Array xs) ->
                safeLast . mapMaybe stringValue $ V.toList xs
              Just (_, String t) -> Just t
              _ -> Nothing
          inputType = expandText =<< inputTypeText
          thirteen expansionResult k v = do
            case join $ Map.lookup k expandedKeysMap of
              _ | k == "@context" -> pure expansionResult
              Nothing -> pure expansionResult
              Just expandedProperty -> do
                expandedValue <- newIORef Null
                result <- newIORef expansionResult
                continue <- newIORef False
                changeExpansionResult <- newIORef (id :: Map (Either Keyword IRI) a -> Map (Either Keyword IRI) a)
                case expandedProperty of
                  Left expandedKeyword -> do
                    when (activeProperty == Just "@reverse") $ throwIO InvalidReversePropertyMap
                    when
                      ( expandedKeyword `notElem` [KeywordIncluded, KeywordType]
                          && isJust (Map.lookup (Left expandedKeyword) expansionResult)
                      )
                      $ throwIO CollidingKeywords
                    case expandedKeyword of
                      KeywordId ->
                        case v of
                          String s -> do
                            let expanded = expandedIriToJSON <$> expandText s
                                framed =
                                  if frameExpansion'
                                    then toJSON [expanded]
                                    else toJSON expanded
                            writeIORef expandedValue framed
                          Array (allStrings -> Just xs) | frameExpansion' -> do
                            -- Not sure if this treatment of nulls is correct
                            writeIORef expandedValue . toJSON . map (fmap expandedIriToJSON . expandText) $ V.toList xs
                          Object (KM.null -> True) | frameExpansion' -> do
                            writeIORef expandedValue (Array (V.singleton (Object mempty)))
                          _ -> throwIO InvalidIdValue
                      KeywordType -> do
                        let expandType = expandIRIWithActiveContext True True typeScopedCtx
                        computedType <- case v of
                          String str -> do
                            let expandedStr = expandType =<< termDefKeyOrKeywordFromText str
                            pure $ toJSON (expandedIriToJSON <$> expandedStr)
                          Array (allStrings -> Just xs) -> do
                            let expandedXS = (fmap expandedIriToJSON . expandType <=< termDefKeyOrKeywordFromText) <$> xs
                            pure $ toJSON expandedXS
                          Object (KM.null -> True) | frameExpansion' -> do
                            pure v
                          Object (KM.lookup "@default" -> Just (iriValue -> Just iri)) | frameExpansion' -> do
                            -- 13.4.4.3 says that the _value_ has to be IRI  expanded,
                            -- but that doesn't type check, so expanding the "@default" here.
                            let expandedIRI = expandType (Right (KeyIRI iri))
                            pure $ object ["@default" .= toJSON (expandedIriToJSON <$> expandedIRI)]
                          _ ->
                            throwIO InvalidTypeValue
                        case Map.lookup (Left KeywordType) expansionResult of
                          Nothing -> writeIORef expandedValue computedType
                          Just t ->
                            case computedType of
                              Array xs -> writeIORef expandedValue $ Array (V.cons t xs)
                              ex -> writeIORef expandedValue $ Array (V.fromList [t, ex])
                      KeywordGraph -> do
                        -- Not really sure what 'fromMap' should be
                        expanded <- expand mgr activeCtx4 (Just "@graph") v baseUrl frameExpansion' ordered fromMap
                        -- 13.4.5 says we have to ensure that this is an array of maps, but doesn't say
                        -- what to do when 'expand' doesn't return a map or array of maps
                        writeIORef expandedValue $
                          case expanded of
                            Array _ -> expanded
                            _ -> Array (V.singleton expanded)
                      KeywordIncluded -> do
                        -- If processingmode is 1.0 -> continue. BUT HOW WHERE DO I GET THE PROCESSING MODE FROM!!!
                        -- Not really sure what 'fromMap' should be
                        expanded <- expand mgr activeCtx Nothing v baseUrl frameExpansion' ordered fromMap
                        let expandedArr = case expanded of
                              Array xs -> xs
                              _ -> V.singleton expanded
                        unless (V.null $ V.filter (not . isNodeObject) expandedArr) $ do
                          throwIO InvalidIncludedValue

                        writeIORef expandedValue $
                          case Map.lookup (Left KeywordType) expansionResult of
                            Nothing -> Array expandedArr
                            Just prev -> Array $ V.cons prev expandedArr
                      KeywordValue -> do
                        computed <- case v of
                          _ | inputType == Just (Left KeywordJson) -> do
                            pure v
                          Array (allScalars -> Just _) | frameExpansion' -> do
                            -- spec says this should be an array of one or more string values,
                            -- I don't know what to do when there are no values here or there
                            -- are some non-string scalars.
                            pure v
                          Object (KM.null -> True) | frameExpansion' -> do
                            pure $ Array $ V.singleton v
                          Array _ ->
                            throwIO InvalidValueObjectValue
                          Object _ ->
                            throwIO InvalidValueObjectValue
                          _ | frameExpansion' -> do
                            pure $ Array $ V.singleton v
                          _ -> do
                            pure v
                        case computed of
                          Null -> do
                            writeIORef continue True
                            writeIORef changeExpansionResult $ Map.insert (Left KeywordValue) Null
                          _ -> writeIORef expandedValue computed
                      KeywordLanguage -> do
                        writeIORef expandedValue
                          =<< case v of
                            Array (allStrings -> Just _) | frameExpansion' -> pure v
                            Object (KM.null -> True) | frameExpansion' -> pure $ Array $ V.singleton v
                            String _ | frameExpansion' -> pure $ Array $ V.singleton v
                            String _ -> pure v
                            _ -> throwIO InvalidLanguageTaggedString
                      KeywordDirection -> do
                        -- If processing mode is json-ld-1.0, continue with the next key from element.
                        writeIORef expandedValue
                          =<< case v of
                            String s ->
                              if s == "ltr" || s == "rtl"
                                then
                                  pure $
                                    if frameExpansion'
                                      then Array $ V.singleton v
                                      else v
                                else throwIO InvalidBaseDirection
                            Array (allStrings -> Just _) | frameExpansion' -> pure v
                            Object (KM.null -> True) | frameExpansion' -> pure $ Array $ V.singleton v
                            _ -> throwIO InvalidBaseDirection
                      KeywordIndex -> do
                        case v of
                          String _ -> writeIORef expandedValue v
                          _ -> throwIO InvalidIndexValue
                      KeywordList -> do
                        if isNothing activeProperty || activeProperty == Just "@graph"
                          then do
                            writeIORef continue True
                          else do
                            expanded <-
                              expand mgr activeCtx activeProperty v baseUrl frameExpansion' ordered fromMap >>= \case
                                x@(Array _) -> pure x
                                x -> pure $ Array $ V.singleton x
                            writeIORef expandedValue expanded
                      KeywordSet -> do
                        expanded <- expand mgr activeCtx activeProperty v baseUrl frameExpansion' ordered fromMap
                        writeIORef expandedValue expanded
                      KeywordReverse -> do
                        case v of
                          Object _ -> pure ()
                          _ -> throwIO InvalidReverseValue
                        -- 13.4.13.2
                        expanded <- expand mgr activeCtx (Just "@reverse") v baseUrl frameExpansion' ordered fromMap
                        writeIORef expandedValue expanded
                        -- 13.4.13.3
                        case expanded of
                          Object expandedObject ->
                            case KM.lookup "@reverse" expandedObject of
                              Just (Object reversedTwice) -> do
                                -- 13.4.13.3
                                _ <-
                                  Map.traverseWithKey
                                    ( \property item ->
                                        modifyIORef result (addValue True property item)
                                    )
                                    (Map.mapKeys (parseKeywordOrTerm . Key.toText) $ KM.toMap reversedTwice)
                                undefined
                              _ -> pure ()
                             
                          _ -> undefined
                      _ -> pure ()
                  _ -> pure ()
                readIORef result
      undefined

parseKeywordOrTerm :: Text -> Either Keyword Term
parseKeywordOrTerm = undefined

isNodeObject :: Value -> Bool
isNodeObject (Object o) =
  let keysSet = Set.fromList (KM.keys o)
   in Set.null (keysSet `Set.intersection` Set.fromList ["@value", "@list", "@set"])
        || keysSet `Set.isSubsetOf` Set.fromList ["@context", "@graph"]
isNodeObject _ = False

foldMapM :: (Monoid b, Monad m, Foldable f) => (a -> m b) -> f a -> m b
foldMapM f xs = foldr step return xs mempty
  where
    step x r z = f x >>= \y -> r $! z `mappend` y

termDefKeyOrKeywordFromText :: Text -> Maybe (Either Keyword TermDefKey)
termDefKeyOrKeywordFromText t =
  (Left <$> parseKeyword t)
    <|> (Right <$> termDefKeyFromText t)

resultToMaybe :: Result a -> Maybe a
resultToMaybe (Error _) = Nothing
resultToMaybe (Success x) = Just x

safeLast :: [a] -> Maybe a
safeLast [] = Nothing
safeLast [x] = Just x
safeLast (_ : xs) = safeLast xs

stringValue :: Value -> Maybe Text
stringValue (String t) = Just t
stringValue _ = Nothing

data ExpansionError
  = InvalidReversePropertyMap
  | CollidingKeywords
  | InvalidIdValue
  | InvalidTypeValue
  | InvalidIncludedValue
  | InvalidValueObjectValue
  | InvalidLanguageTaggedString
  | InvalidBaseDirection
  | InvalidIndexValue
  | InvalidReverseValue
  deriving (Show, Eq)

instance Exception ExpansionError

addValue :: Ord k => Bool -> k -> Value -> Map k Value -> Map k Value
addValue asArray k val m =
  let toVector = \case
        Array v -> v
        v -> V.singleton v
      m' =
        if asArray
          then Map.insertWith (const $ Array . toVector) k (Array mempty) m
          else m
   in case val of
        Array xs -> foldl' (flip (addValue asArray k)) m' xs
        _ -> Map.insertWith (\new old -> Array $ toVector old <> V.singleton new) k val m'

allScalars :: Traversable t => t Value -> Maybe (t Value)
allScalars vs =
  let toScalar = \case
        Array _ -> Nothing
        Object _ -> Nothing
        x -> Just x
   in traverse toScalar vs

allStrings :: Traversable t => t Value -> Maybe (t Text)
allStrings vs =
  let toString = \case
        String s -> Just s
        _ -> Nothing
   in traverse toString vs

iriValue :: Value -> Maybe IRI
iriValue (String s) = iriFromText s
iriValue _ = Nothing
