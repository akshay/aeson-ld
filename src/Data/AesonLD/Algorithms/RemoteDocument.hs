{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Data.AesonLD.Algorithms.RemoteDocument where

import Control.Exception
import Control.Monad.Except
import Control.Monad.Reader
import Data.AesonLD.Algorithms.Utils
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import Data.CaseInsensitive (CI, foldedCase)
import Data.Either (rights)
import Data.RDF.Types (IRI (..))
import Data.Text (Text)
import qualified Data.Text.Encoding as Text
import Lens.Micro
import qualified Network.HTTP.Client as HTTP
import qualified Network.HTTP.Link as HTTP
import Network.HTTP.Media ((/.), (//), (/:))
import qualified Network.HTTP.Media as HTTP
import qualified Network.HTTP.Types as HTTP
import Network.URI (URI (..))
import qualified Network.URI as URI

newtype HttpDocumentLoader a = HttpDocumentLoader (ReaderT HTTP.Manager IO a)
  deriving newtype (Functor, Applicative, Monad, MonadReader HTTP.Manager, MonadIO)

runHttpDocumentLoader :: HTTP.Manager -> HttpDocumentLoader a -> IO a
runHttpDocumentLoader mgr (HttpDocumentLoader action) =
  runReaderT action mgr

-- | According to https://www.w3.org/TR/json-ld11-api/#remote-document-and-context-retrieval
instance RemoteDocumentLoader HttpDocumentLoader where
  loadDocumentBS :: URI -> LoadDocumentOptions -> HttpDocumentLoader (Either DocumentLoadError (RemoteDocument LBS.ByteString))
  loadDocumentBS uri opts = runExceptT $ do
    let addProfile = maybe id (\p -> (/: ("profile", Text.encodeUtf8 p))) (ldoProfile opts)
        acceptedTypes = [addProfile ("application" // "ld+json"), "application" // "json"]
        req =
          (HTTP.requestFromURI_ uri)
            { HTTP.requestHeaders = [(HTTP.hAccept, HTTP.renderHeader acceptedTypes)]
            }
    mgr <- ask
    resHistory <- liftIO $ HTTP.withResponseHistory req mgr pure
    let res = HTTP.hrFinalResponse resHistory
        finalReq = HTTP.hrFinalRequest resHistory
        mContentType = lookup HTTP.hContentType $ HTTP.responseHeaders res
        mMediaType = HTTP.parseAccept =<< mContentType
        mProfile = (/. "profile") =<< mMediaType
        contentTypeText = mMediaType <&> \mt -> Text.decodeUtf8 . foldedCase $ HTTP.mainType mt <> "/" <> HTTP.subType mt
        ctIsJson = flip (maybe False) mMediaType $ \mt ->
          (HTTP.mainType mt == "application" && HTTP.subType mt == "json")
            || ("+json" `BS.isSuffixOf` foldedCase (HTTP.subType mt))
        ctIsJsonLD = flip (maybe False) mMediaType $ \mt ->
          HTTP.mainType mt == "application" && HTTP.subType mt == "ld+json"
        linkHeadersBS = map snd $ filter ((== "Link") . fst) $ HTTP.responseHeaders res
        linkHeaders = concat $ rights $ map (HTTP.parseLinkHeaderBS' @URI) linkHeadersBS
        isAlternateLink = elem "alternate" . lookupLinkParam HTTP.Rel
        isTypeJSONLD = elem "application/ld+json" . lookupLinkParam HTTP.ContentType
        alternateJSONLDHeaders = filter (\h -> isAlternateLink h && isTypeJSONLD h) linkHeaders
        contextLinkURI = "http://www.w3.org/ns/json-ld#context"
    case alternateJSONLDHeaders of
      ((HTTP.Link altURI _) : _)
        | not ctIsJson ->
            ExceptT $ loadDocumentBS (altURI `URI.relativeTo` uri) opts
      _ -> do
        mContextUrl <- case filter (elem contextLinkURI . lookupLinkParam HTTP.Rel) linkHeaders of
          [contextLink] | ctIsJson && not ctIsJsonLD -> pure . Just $ HTTP.href contextLink
          (_ : _) -> throwError MultipleContextLinkHeaders
          _ | not ctIsJson -> throwError LoadingDocumentFailed
          [] -> pure Nothing
        doc <- liftIO $ LBS.fromChunks <$> HTTP.brConsume (HTTP.responseBody res)
        pure $
          RemoteDocument
            { rdContentType = contentTypeText,
              rdContextUrl = uriToIRI <$> mContextUrl,
              rdDocument = doc,
              rdDocumentUrl = uriToIRI $ HTTP.getUri finalReq,
              rdProfile = mProfile
            }

lookupLinkParam :: HTTP.LinkParam -> HTTP.Link a -> [Text]
lookupLinkParam param =
  map snd . filter ((== param) . fst) . HTTP.linkParams

data Foo = LoadWith URI | RespondWith (Either DocumentLoadError (RemoteDocument LBS.ByteString))

class RemoteDocumentLoader m where
  loadDocumentBS :: URI -> LoadDocumentOptions -> m (Either DocumentLoadError (RemoteDocument LBS.ByteString))

data LoadDocumentOptions = LoadDocumentOptions
  { ldoExtractAllScripts :: Bool,
    ldoProfile :: Maybe Text,
    ldoRequestProfile :: Maybe [Text]
  }

contextProfile :: Text
contextProfile = "http://www.w3.org/ns/json-ld#context"

loadContextOptions :: LoadDocumentOptions
loadContextOptions = LoadDocumentOptions False (Just contextProfile) (Just [contextProfile])

data RemoteDocument a = RemoteDocument
  { rdContentType :: Maybe Text,
    -- | If available, the value of the HTTP Link Header [RFC8288] using the
    -- http://www.w3.org/ns/json-ld#context link relation in the response. If
    -- the response's Content-Type is application/ld+json, the HTTP Link Header
    -- is ignored. If multiple HTTP Link Headers using the
    -- http://www.w3.org/ns/json-ld#context link relation are found,
    -- 'loadContext' fails with a 'MultipleContextLinkHeaders'.
    rdContextUrl :: Maybe IRI,
    rdDocument :: a,
    rdDocumentUrl :: IRI,
    rdProfile :: Maybe (CI ByteString)
  }
  deriving (Functor, Foldable, Traversable)

data DocumentLoadError
  = MultipleContextLinkHeaders
  | DocumentParseError String
  | NoHostnameInIRI IRI
  | InvalidScheme Text
  | LoadingDocumentFailed
  deriving (Show, Eq)

instance Exception DocumentLoadError
