{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Data.AesonLD.Algorithms.ContextProcessing.Internal where

import Control.Exception
import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import qualified Data.Aeson as Aeson
import Data.AesonLD.Algorithms.RemoteDocument
import Data.AesonLD.Algorithms.Utils
import Data.AesonLD.ContextDefinition
import Data.AesonLD.Keyword
import Data.AesonLD.Term
import Data.Bifunctor (first)
import Data.IORef
import Data.Map (Map)
import qualified Data.Map as Map
import Data.RDF.Types (IRI (..))
import Data.Text (Text)
import Lens.Micro
import qualified Network.HTTP.Client as HTTP

-- * Types

data ActiveContext = ActiveContext
  { acCurrentBaseIRI :: Maybe IRI,
    acOriginalBaseURL :: IRI,
    acInverseContext :: InverseContext,
    acTermDefs :: Map TermDefKey ActiveTermDefinition,
    acVocab :: Maybe IRI,
    acDefaultLanguage :: Maybe Language,
    acDefaultDirection :: Maybe Direction,
    acPreviousContext :: Maybe ActiveContext
  }
  deriving (Show)

initActiveContext :: IRI -> Maybe ActiveContext -> ActiveContext
initActiveContext base mPrevContext =
  ActiveContext
    { acCurrentBaseIRI = Just base,
      acOriginalBaseURL = base,
      acInverseContext = mempty,
      acTermDefs = mempty,
      acVocab = Nothing,
      acDefaultLanguage = Nothing,
      acDefaultDirection = Nothing,
      acPreviousContext = mPrevContext
    }

currentBaseIriL :: Lens' ActiveContext (Maybe IRI)
currentBaseIriL f ac = fmap (\a' -> ac {acCurrentBaseIRI = a'}) (f (acCurrentBaseIRI ac))

originalBaseUrlL :: Lens' ActiveContext IRI
originalBaseUrlL f ac = fmap (\a' -> ac {acOriginalBaseURL = a'}) (f (acOriginalBaseURL ac))

inverseContextL :: Lens' ActiveContext InverseContext
inverseContextL f ac = fmap (\a' -> ac {acInverseContext = a'}) (f (acInverseContext ac))

termDefsL :: Lens' ActiveContext (Map TermDefKey ActiveTermDefinition)
termDefsL f ac = fmap (\a' -> ac {acTermDefs = a'}) (f (acTermDefs ac))

vocabL :: Lens' ActiveContext (Maybe IRI)
vocabL f ac = fmap (\a' -> ac {acVocab = a'}) (f (acVocab ac))

defaultLanguageL :: Lens' ActiveContext (Maybe Language)
defaultLanguageL f ac = fmap (\a' -> ac {acDefaultLanguage = a'}) (f (acDefaultLanguage ac))

defaultDirectionL :: Lens' ActiveContext (Maybe Direction)
defaultDirectionL f ac = fmap (\a' -> ac {acDefaultDirection = a'}) (f (acDefaultDirection ac))

previousContextL :: Lens' ActiveContext (Maybe ActiveContext)
previousContextL f ac = fmap (\a' -> ac {acPreviousContext = a'}) (f (acPreviousContext ac))

-- | TODO: Make this type better
newtype InverseContext = InverseContext (Map Text Term)
  deriving newtype (Semigroup, Monoid, Show)

data ActiveTermDefinition = ActiveTermDefinition
  { atdIRI :: Maybe (Either Keyword IRI),
    atdPrefix :: Bool,
    atdProtected :: Bool,
    atdReverse :: Bool,
    -- atdBaseURL :: Maybe IRI,
    -- atdContext :: Maybe ActiveContext,
    atdLocalContext :: Maybe (IRI, LocalContext),
    atdContainerMapping :: Maybe Container,
    atdDirection :: Maybe Direction,
    atdIndexMapping :: Maybe Index,
    atdLanguageMapping :: Maybe Language,
    atdNestMapping :: Maybe Nest,
    atdTypeMapping :: Maybe ActiveType
  }
  deriving (Show, Eq)

data ActiveType
  = TypeKeyword KeywordType
  | TypeIRI IRI
  deriving (Show, Eq)

data KeywordType
  = TypeJson
  | TypeNone
  | TypeId
  | TypeVocab
  deriving (Show, Eq)

iriL :: Lens' ActiveTermDefinition (Maybe (Either Keyword IRI))
iriL f atd = fmap (\a' -> atd {atdIRI = a'}) (f (atdIRI atd))

prefixL :: Lens' ActiveTermDefinition Bool
prefixL f atd = fmap (\a' -> atd {atdPrefix = a'}) (f (atdPrefix atd))

protectedL :: Lens' ActiveTermDefinition Bool
protectedL f atd = fmap (\a' -> atd {atdProtected = a'}) (f (atdProtected atd))

reverseL :: Lens' ActiveTermDefinition Bool
reverseL f atd = fmap (\a' -> atd {atdReverse = a'}) (f (atdReverse atd))

-- baseURLL :: Lens' ActiveTermDefinition (Maybe IRI)
-- baseURLL f atd = fmap (\a' -> atd {atdBaseURL = a'}) (f (atdBaseURL atd))

-- contextL :: Lens' ActiveTermDefinition (Maybe ActiveContext)
-- contextL f atd = fmap (\a' -> atd {atdContext = a'}) (f (atdContext atd))

localContextL :: Lens' ActiveTermDefinition (Maybe (IRI, LocalContext))
localContextL f atd = fmap (\a' -> atd {atdLocalContext = a'}) (f (atdLocalContext atd))

containerMappingL :: Lens' ActiveTermDefinition (Maybe Container)
containerMappingL f atd = fmap (\a' -> atd {atdContainerMapping = a'}) (f (atdContainerMapping atd))

directionL :: Lens' ActiveTermDefinition (Maybe Direction)
directionL f atd = fmap (\a' -> atd {atdDirection = a'}) (f (atdDirection atd))

indexMappingL :: Lens' ActiveTermDefinition (Maybe Index)
indexMappingL f atd = fmap (\a' -> atd {atdIndexMapping = a'}) (f (atdIndexMapping atd))

languageMappingL :: Lens' ActiveTermDefinition (Maybe Language)
languageMappingL f atd = fmap (\a' -> atd {atdLanguageMapping = a'}) (f (atdLanguageMapping atd))

nestMappingL :: Lens' ActiveTermDefinition (Maybe Nest)
nestMappingL f atd = fmap (\a' -> atd {atdNestMapping = a'}) (f (atdNestMapping atd))

typeMappingL :: Lens' ActiveTermDefinition (Maybe ActiveType)
typeMappingL f atd = fmap (\a' -> atd {atdTypeMapping = a'}) (f (atdTypeMapping atd))

-- * Processsing Monad

newtype ContextProcessing r a = ContextProcessing {unwrapCP :: ReaderT (Env r) IO a}
  deriving newtype (Functor, Applicative, Monad, MonadIO, MonadReader (Env r))

instance MonadState (ProcessingState r) (ContextProcessing r) where
  get = asks stateRef >>= (liftIO . readIORef)
  put x = asks stateRef >>= liftIO . flip writeIORef x

instance MonadError ContextProcessingError (ContextProcessing r) where
  throwError = liftIO . throwIO
  catchError action f = do
    s <- ask
    liftIO $ catch (runContextProcessing s action) (runContextProcessing s . f)

runContextProcessing :: Env r -> ContextProcessing r a -> IO a
runContextProcessing env action = do
  runReaderT (unwrapCP action) env

execContextProcessing :: Env r -> ContextProcessing r () -> IO (ProcessingState r)
execContextProcessing env action = do
  runReaderT (unwrapCP action) env
  readIORef (stateRef env)

processAnother :: r1 -> ContextProcessing r1 () -> ContextProcessing r2 r1
processAnother initState action = do
  mode <- asks processingMode
  lc <- asks loadContext
  r2State <- get
  -- The remoteCtxDerefs won't actually get updated, maybe they shouldn't even be exposed?
  r1State <- liftIO $ newIORef $ ProcessingState initState (propagate r2State) (remoteCtxDerefs r2State)
  liftIO $ result <$> execContextProcessing (Env mode lc r1State) action

data Env a = Env
  { processingMode :: ProcessingMode,
    loadContext :: IRI -> LoadDocumentOptions -> IO (Either DocumentLoadError (RemoteDocument LocalContext)),
    stateRef :: IORef (ProcessingState a)
  }

newEnv :: ProcessingMode -> HTTP.Manager -> ProcessingState r -> IO (Env r)
newEnv mode mgr initState = do
  s <- newIORef initState
  let loader iri opts = runHttpDocumentLoader mgr $ do
        docBS <- loadDocumentBS (iriToURI iri) opts
        let eithDoc = first DocumentParseError . traverse Aeson.eitherDecode =<< docBS
        case eithDoc of
          Left err -> pure $ Left err
          Right doc -> do
            liftIO $ modifyIORef s (remoteCtxDerefsL %~ Map.insert iri doc)
            pure $ Right doc
  pure $ Env mode loader s

processingModeL :: Lens' (Env r) ProcessingMode
processingModeL f env = fmap (\p' -> env {processingMode = p'}) (f (processingMode env))

data ProcessingMode = ProcessingMode1_0 | ProcessinMode1_1
  deriving (Show, Eq)

data ProcessingState a = ProcessingState
  { result :: a,
    propagate :: Bool,
    remoteCtxDerefs :: Map IRI (RemoteDocument LocalContext)
  }

resultL :: Lens' (ProcessingState r) r
resultL f cps = fmap (\a' -> cps {result = a'}) (f (result cps))

propagateL :: Lens' (ProcessingState r) Bool
propagateL f cps = fmap (\a' -> cps {propagate = a'}) (f (propagate cps))

remoteCtxDerefsL :: Lens' (ProcessingState r) (Map IRI (RemoteDocument LocalContext))
remoteCtxDerefsL f cps = fmap (\a' -> cps {remoteCtxDerefs = a'}) (f (remoteCtxDerefs cps))

data ContextProcessingError
  = InvalidContextNullification
  | ContextOverflow
  | LoadingRemoteContextFailed DocumentLoadError
  | ProcessingModeConflict
  | InvalidVocabMapping
  | InvalidContextEntry
  | InvalidRemoteContext
  | InvalidBaseIRI
  | CyclicIRIMapping
  | InvalidTermDefinition
  | KeywordRedefinition
  | InvalidTypeMapping
  | InvalidReverseProperty
  | InvalidIRIMapping
  | InvalidContainerMapping
  | ProtectedTermRedefinition
  | InvalidScopedContext
  deriving (Show, Eq)

instance Exception ContextProcessingError

data IRIExpansionError = IRIExpansionError
  deriving (Show, Eq)

instance Exception IRIExpansionError
