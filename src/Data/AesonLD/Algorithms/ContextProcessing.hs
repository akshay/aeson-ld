{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Data.AesonLD.Algorithms.ContextProcessing where

import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import Data.Aeson (eitherDecodeFileStrict)
import Data.AesonLD.Algorithms.ContextProcessing.Internal
import Data.AesonLD.Algorithms.RemoteDocument
import Data.AesonLD.ContextDefinition
import Data.AesonLD.Keyword
import Data.AesonLD.Term
import Data.ByteString.Builder (toLazyByteString)
import qualified Data.ByteString.Lazy as LBS
import Data.Functor.Identity (Identity (..))
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe, isJust)
import Data.RDF.Encoder.Common (encodeIRI)
import Data.RDF.Types (IRI)
import Data.Text (Text)
import Lens.Micro
import Lens.Micro.Extras (view)
import qualified Network.HTTP.Client as HTTP

test :: IO ActiveContext
test = do
  mgr <- HTTP.newManager HTTP.defaultManagerSettings
  Right localCtx <- eitherDecodeFileStrict @LocalContext "person.jsonld"
  let baseActiveCtx = initActiveContext "https://json-ld.org/contexts/person.jsonld" Nothing
  newActiveContextDefaults mgr baseActiveCtx localCtx "https://json-ld.org/contexts/person.jsonld"

newActiveContextDefaults ::
  -- | Used to fetch remote contexts
  HTTP.Manager ->
  -- | Previous active context
  ActiveContext ->
  -- | Local context
  LocalContext ->
  -- | Base URL
  IRI ->
  IO ActiveContext
newActiveContextDefaults mgr activeCtx localCtx baseIRI =
  newActiveContext ProcessinMode1_1 mgr activeCtx localCtx baseIRI [] False True True

newActiveContext ::
  ProcessingMode ->
  -- | Used to fetch remote contexts
  HTTP.Manager ->
  -- | Previous active context
  ActiveContext ->
  -- | Local context
  LocalContext ->
  -- | Base URL
  IRI ->
  -- | Remove Contexts, used to detect if there are cyclical context inclusion
  [IRI] ->
  -- | Override protected
  Bool ->
  -- | Propagate
  Bool ->
  -- | Validate scoped context, used to limit recursion
  Bool ->
  IO ActiveContext
newActiveContext mode mgr activeCtx localCtx baseIRI rmtCtxs overrideProtected prpgt validate = do
  -- 1) Initialize result to the result of cloning active context, with inverse context set to null..
  let initialState = ProcessingState (activeCtx & inverseContextL .~ mempty) prpgt mempty
  env <- newEnv mode mgr initialState
  fmap result . execContextProcessing env $
    newActiveContext' activeCtx localCtx baseIRI rmtCtxs overrideProtected validate

newActiveContext' ::
  ActiveContext -> LocalContext -> IRI -> [IRI] -> Bool -> Bool -> ContextProcessing ActiveContext ()
newActiveContext' activeCtx localCtx baseIRI rmtCtxs overrideProtected validateScopedCtx = do
  -- 2) If local context is an object containing the member @propagate, its
  -- value MUST be boolean true or false, set propagate to that value.
  case localCtx ^. contextListL of
    [Set (EmbeddedContext cd)] ->
      forM_ (cd ^. cdPropagateL) (propagateL .=)
    _ -> pure ()

  -- 3) If propagate is false, and result does not have a previous context, set
  -- previous context in result to active context.
  unlessM (gets . view $ propagateL) $
    resultL . previousContextL .= Just activeCtx

  -- 5) For each item context in local context:
  mapM_ (processContextEntry activeCtx baseIRI rmtCtxs overrideProtected validateScopedCtx) (localCtx ^. contextListL)

maxRemoteIRIs :: Int
maxRemoteIRIs = 3

processContextEntry ::
  ActiveContext -> IRI -> [IRI] -> Bool -> Bool -> Resettable SingleContext -> ContextProcessing ActiveContext ()
processContextEntry activeCtx baseIRI rmtCtxs overrideProtected validateScopedCtx ctx = do
  case ctx of
    Reset ->
      processNullContext activeCtx overrideProtected
    Set (RemoteContext remoteIRI) ->
      processIRIContext rmtCtxs overrideProtected validateScopedCtx remoteIRI
    Set (EmbeddedContext ctxDef) -> processEmbeddedContext activeCtx baseIRI rmtCtxs overrideProtected ctxDef

processNullContext :: ActiveContext -> Bool -> ContextProcessing ActiveContext ()
processNullContext activeCtx overrideProtected = do
  -- 5.1.1) If override protected is false and active context contains any
  -- protected term definitions, an invalid context nullification has been
  -- detected and processing is aborted.
  when (not overrideProtected && any (^. protectedL) (activeCtx ^. termDefsL)) $
    throwError InvalidContextNullification

  -- 5.1.2) Initialize result as a newly-initialized active context, setting
  -- both base IRI and original base URL to the value of original base URL
  -- in active context, and, if propagate is false, previous context in
  -- result to the previous value of result.
  modify $ \prevState ->
    let mPrevCtx =
          if prevState ^. propagateL
            then Just (prevState ^. resultL)
            else Nothing
     in prevState & resultL .~ initActiveContext (activeCtx ^. originalBaseUrlL) mPrevCtx

processIRIContext :: [IRI] -> Bool -> Bool -> IRI -> ContextProcessing ActiveContext ()
processIRIContext rmtCtxs overrideProtected validateScopedCtx remoteIRI = do
  -- 5.2.2) If validate scoped context is false, and remote contexts already
  -- includes context do not process context further and continue to any
  -- next context in local context.
  let alreadyProcessed = remoteIRI `elem` rmtCtxs
  unless (not validateScopedCtx && alreadyProcessed) $ do
    -- 5.2.3) If the number of entries in the remote contexts array exceeds
    -- a processor defined limit, a context overflow error has been detected
    -- and processing is aborted; otherwise, add context to remote contexts.
    when (length rmtCtxs > maxRemoteIRIs) $
      throwError ContextOverflow

    -- 5.2.4) If context was previously dereferenced, then the processor
    -- MUST NOT do a further dereference, and context is set to the
    -- previously established internal representation: set context document
    -- to the previously dereferenced document, and set loaded context to
    -- the value of the @context entry from the document in context
    -- document.
    alreadyDerefed <- gets . view $ remoteCtxDerefsL . to (Map.lookup remoteIRI)
    remoteCtxDoc <- case alreadyDerefed of
      Just derefed -> pure derefed
      Nothing -> do
        -- 5.2.5) Otherwise, set context document to the RemoteDocument obtained
        -- by dereferencing context using the LoadDocumentCallback, passing
        -- context for url, and http://www.w3.org/ns/json-ld#context for profile
        -- and for requestProfile.
        env <- ask
        eithCtx <- liftIO $ loadContext env remoteIRI loadContextOptions
        either (throwError . LoadingRemoteContextFailed) pure eithCtx

    -- 5.2.6) Set result to the result of recursively calling this
    -- algorithm, passing result for active context, loaded context for
    -- local context, the documentUrl of context document for base URL, a
    -- copy of remote contexts, and validate scoped context.
    currentResult <- gets (view resultL)
    newActiveContext' currentResult (rdDocument remoteCtxDoc) (rdDocumentUrl remoteCtxDoc) (remoteIRI : rmtCtxs) overrideProtected validateScopedCtx

processEmbeddedContext :: ActiveContext -> IRI -> [IRI] -> Bool -> ContextDefinition -> ContextProcessing ActiveContext ()
processEmbeddedContext activeCtx baseIRI rmtCtxs overrideProt ctxDef = do
  -- 5.5.2) If processing mode is set to json-ld-1.0, a processing mode conflict
  -- error has been detected and processing is aborted.
  env <- ask
  when (ctxDef ^. cdVersionL == Just Version1_1 && processingMode env == ProcessingMode1_0) $
    throwError ProcessingModeConflict

  ctxDefWithImport <- case ctxDef ^. cdImportL of
    Just importIRIRef -> do
      when (processingMode env == ProcessingMode1_0) $
        throwError InvalidContextEntry
      let importIRI = resolveIRIReference importIRIRef baseIRI
      eithCtx <- liftIO $ loadContext env importIRI loadContextOptions
      importedDef <- either (throwError . LoadingRemoteContextFailed) (pure . rdDocument) eithCtx
      case importedDef of
        ContextList [Set (EmbeddedContext importedCtxDef)] -> do
          -- 5.6.7) If import context has a @import entry, an invalid context
          -- entry error has been detected and processing is aborted.
          when (importedCtxDef ^. cdImportL . to isJust) $
            throwError InvalidContextEntry
          pure (importContextDef ctxDef importedCtxDef)
        _ ->
          -- 5.6.6) If the dereferenced document has no top-level map with an
          -- @context entry, or if the value of @context is not a context
          -- definition (i.e., it is not an map), an invalid remote context has
          -- been detected and processing is aborted; otherwise, set import
          -- context to the value of that entry.
          throwError InvalidRemoteContext
    Nothing -> pure ctxDef

  -- 5.7) If context has an @base entry and remote contexts is empty, i.e., the
  -- currently being processed context is not a remote context:
  --
  -- 5.7.1) Initialize value to the value associated with the @base entry.
  --
  -- 5.7.2) If value is null, remove the base IRI of result.
  --
  -- 5.7.3) Otherwise, if value is an IRI, the base IRI of result is set to
  -- value.
  --
  -- 5.7.4) Otherwise, if value is a relative IRI reference and the base IRI of
  -- result is not null, set the base IRI of result to the result of resolving
  -- value against the current base IRI of result.
  --
  -- 5.7.5) Otherwise, an invalid base IRI error has been detected and
  -- processing is aborted.
  when (null rmtCtxs) $
    withJust (ctxDefWithImport ^. cdBaseL) $ \case
      Reset -> resultL . currentBaseIriL .= Nothing
      Set (Base newBaseRef) -> do
        use (resultL . currentBaseIriL) >>= \case
          Nothing -> throwError InvalidBaseIRI
          Just currentBaseIri ->
            resultL . currentBaseIriL ?= resolveIRIReference newBaseRef currentBaseIri

  -- 5.8) If context has an @vocab entry:
  --
  -- 5.8.1) Initialize value to the value associated with the @vocab entry.
  --
  -- 5.8.2) If value is null, remove any vocabulary mapping from result.
  --
  -- 5.8.3) Otherwise, if value is an IRI or blank node identifier, the
  -- vocabulary mapping of result is set to the result of IRI expanding value
  -- using true for document relative . If it is not an IRI, or a blank node
  -- identifier, an invalid vocab mapping error has been detected and processing
  -- is aborted.
  let vocabToTermDefKey = \case
        VocabIRIReference (RelativeIRI ref) -> KeyRelativeIRI ref
        VocabIRIReference (AbsoluteIRI iri) -> KeyIRI iri
        VocabCompact c -> KeyCompact c
  withJust (ctxDefWithImport ^. cdVocabL) $ \case
    Reset ->
      resultL . vocabL .= Nothing
    Set v -> do
      mExpanded <- expandIRIInActiveContext True False Nothing mempty $ Right (vocabToTermDefKey v)
      case mExpanded of
        Just (Right expandedIRI) -> resultL . vocabL ?= expandedIRI
        _ -> throwError InvalidVocabMapping

  withJust (ctxDefWithImport ^. cdLanguageL) $ \l ->
    resultL . defaultLanguageL .= resettableToMaybe l

  withJust (ctxDefWithImport ^. cdDirectionL) $ \d -> do
    when (processingMode env == ProcessingMode1_0) $
      throwError InvalidContextEntry
    resultL . defaultDirectionL .= resettableToMaybe d

  -- NOTE: The previous context is actually set earlier in this algorithm; these
  -- steps exist for error checking only.
  withJust (ctxDefWithImport ^. cdPropagateL) . const $ do
    when (processingMode env == ProcessingMode1_0) $
      throwError InvalidContextEntry

  mapM_
    ( \(key, def) -> do
        res <- use resultL
        let prot = fromMaybe False (join $ def ^? _Set . _TDExpanded . etdProtectedL)
            defined = True <$ res ^. termDefsL
        createTermDefinition ctxDefWithImport rmtCtxs prot overrideProt defined key def
    )
    (ctxDefWithImport ^. cdTermDefinitionsL . to Map.toList)

createTermDefinition ::
  ContextDefinition ->
  [IRI] ->
  -- | Protected
  Bool ->
  -- | Override Protected
  Bool ->
  Map TermDefKey Bool ->
  TermDefKey ->
  Resettable TermDefinition ->
  ContextProcessing ActiveContext ()
createTermDefinition ctxDef rmtCtxs prot overrideProt defined termDefKey mtd = do
  when (Map.lookup termDefKey defined == Just False) $
    throwError CyclicIRIMapping
  let newDefined = Map.insert termDefKey False defined
  let (etd, simpleTerm) = expandTermDefinitiion mtd
  createTermDefinition' ctxDef rmtCtxs prot overrideProt newDefined termDefKey simpleTerm etd

createTermDefinition' ::
  ContextDefinition ->
  [IRI] ->
  -- | Protected
  Bool ->
  -- | Override Protected
  Bool ->
  Map TermDefKey Bool ->
  TermDefKey ->
  -- | Simple term
  Bool ->
  ExpandedTermDefinition ->
  ContextProcessing ActiveContext ()
createTermDefinition' ctxDef rmtCtxs prot overrideProt defined term simpleTerm etd = do
  env <- ask
  let isV1_0 = env ^. processingModeL == ProcessingMode1_0
      throwIfV1_0 e = when isV1_0 $ throwError e
  protected <- withMaybe prot (etd ^. etdProtectedL) $ \p -> do
    throwIfV1_0 InvalidTermDefinition
    pure p

  activeCtx <- gets (view resultL)
  let mkIRI = expandIRIInActiveContext False False (Just ctxDef) defined

  reverseMapping <- withMaybe False (etd ^. etdReverseL) $ \_ -> do
    when (isJust (etd ^. etdIdL) || isJust (etd ^. etdNestL)) $
      throwError InvalidReverseProperty
    pure True

  let iriFromRev r = do
        let revTermDefKey = case r of
              ReverseReserved res -> KeyReserved res
              ReverseCompact c -> KeyCompact c
              ReverseIRI i -> KeyIRI i
              ReverseTerm t -> KeyTerm t
        mkIRI $ Right revTermDefKey
      throwIfContext (Left KeywordContext) = throwError InvalidIRIMapping
      throwIfContext x = pure x
      iriFromId ri = do
        let mkIRIValidate = traverse throwIfContext <=< mkIRI
        case ri of
          Reset -> pure Nothing
          Set (IdKeyword k) -> mkIRIValidate (Left k)
          Set (IdReserved r) -> mkIRIValidate (Right (KeyReserved r))
          Set (IdIRI iri) -> mkIRIValidate (Right (KeyIRI iri))
          Set (IdCompact c) -> mkIRIValidate (Right (KeyCompact c))
          Set (IdTerm t) -> mkIRIValidate (Right (KeyTerm t))
      isTermSameAsId = case term of
        KeyTerm t -> etd ^. etdIdL == Just (Set (IdTerm t))
        KeyCompact c -> etd ^. etdIdL == Just (Set (IdCompact c))
        KeyIRI i -> etd ^. etdIdL == Just (Set (IdIRI i))
        KeyReserved _ -> False
        KeyRelativeIRI _ -> False
      iriFromCompactLocalCtx (CompactIRI prefixText suffix) = do
        let prefixTerm = KeyTerm prefixText
        withJust (ctxDef ^. cdTermDefinitionsL . to (Map.lookup prefixTerm)) $ \prefixDef ->
          createTermDefinition ctxDef rmtCtxs prot overrideProt defined prefixTerm prefixDef
        mPrefixDef <- gets (^. resultL . termDefsL . to (Map.lookup prefixTerm))
        withJust' mPrefixDef $ \def ->
          case def ^. iriL of
            Just (Right i) -> pure . Just . Right $ appendToIRI i suffix
            _ -> pure Nothing
      iriFromRelativeIRI rel =
        maybe (throwError InvalidIRIMapping) (pure . Just)
          =<< mkIRI (Right (KeyRelativeIRI rel))
  iriMapping <-
    if
        | etd ^. etdReverseL . to isJust ->
            withJustOrError InvalidIRIMapping (etd ^. etdReverseL) iriFromRev
        | etd ^. etdIdL . to isJust && not isTermSameAsId ->
            withJust' (etd ^. etdIdL) iriFromId
        | isJust (term ^? _KeyCompact) ->
            withJust' (term ^? _KeyCompact) iriFromCompactLocalCtx
        | isJust (term ^? _KeyIRI) ->
            pure $ term ^? _KeyIRI . to Right
        | isJust (term ^? _KeyRelativeIRI) ->
            withJust' (term ^? _KeyRelativeIRI) iriFromRelativeIRI
        | isJust (activeCtx ^. vocabL) ->
            withJust' (activeCtx ^. vocabL) $ \i ->
              withJust' (term ^? _KeyTerm) $ \t ->
                pure . Just . Right $ appendToIRI i (termToText t)
        | otherwise ->
            throwError InvalidIRIMapping

  containerMapping <- withJust' (etd ^. etdContainerL) $ \case
    Reset -> pure Nothing
    Set c -> do
      when (isJust (c ^? _ContainerGraph) || containerHasType c || contianerHasId c) $
        throwIfV1_0 InvalidContainerMapping
      pure $ Just c

  let defTypeMapping =
        if maybe False containerHasType containerMapping
          then Just $ TypeKeyword TypeId
          else Nothing
      mkKeywordType k =
        case k of
          KeywordId -> pure TypeId
          KeywordVocab -> pure TypeVocab
          KeywordJson -> pure TypeJson
          KeywordNone -> pure TypeNone
          _ -> throwError InvalidTypeMapping
      validateTypeMapping t = do
        let isTooNewTypeKeyword =
              isV1_0 && t `elem` [TypeKeyword TypeJson, TypeKeyword TypeNone]
            isIncompatibleWithContainer =
              maybe False containerHasType containerMapping
                && t `notElem` [TypeKeyword TypeVocab, TypeKeyword TypeId]
        when (isTooNewTypeKeyword || isIncompatibleWithContainer) $
          throwError InvalidTypeMapping
        pure t
  typeMapping <-
    withMaybe defTypeMapping (etd ^. etdTypeL) $ \t -> do
      mIRI <- mkIRI $ case t of
        CompactTypeReserved k -> Right (KeyReserved k)
        CompactTypeIRI i -> Right (KeyIRI i)
        CompactTypeCompact c -> Right (KeyCompact c)
        CompactTypeTerm tm -> Right (KeyTerm tm)
        CompactTypeKeyword k -> Left k
      typeMapping <- case mIRI of
        Nothing -> throwError InvalidTypeMapping
        Just (Left k) -> TypeKeyword <$> mkKeywordType k
        Just (Right i) -> pure $ TypeIRI i
      Just <$> validateTypeMapping typeMapping
  indexMapping <-
    withJust' (etd ^. etdIndexL) $ \i -> do
      when (isV1_0 || maybe False (not . containerHasIndex) containerMapping) $
        throwError InvalidTermDefinition
      pure $ Just i
  localContext <-
    withJust' (etd ^. etdContextL) $ \c -> do
      let baseIRI = acOriginalBaseURL activeCtx
      void . processAnother (initActiveContext baseIRI (Just activeCtx)) $
        newActiveContext' activeCtx c baseIRI rmtCtxs True False
          `catchError` \_ -> throwError InvalidScopedContext
      pure $ Just (baseIRI, c)
  let languageMapping =
        case etd ^. etdTypeL of
          Just _ -> Nothing
          Nothing -> etd ^. etdLanguageL
      directionMapping =
        case etd ^. etdTypeL of
          Just _ -> Nothing
          Nothing -> etd ^. etdDirectionL
  nest <-
    withJust' (etd ^. etdNestL) $ \n -> do
      throwIfV1_0 InvalidTermDefinition
      pure (Just n)
  let iriCanBePrefix =
        case iriMapping of
          Nothing -> False
          Just (Left _) -> False
          Just (Right i) ->
            let genDelims = [":", ",", "?", "#", "[", "]", "@"]
             in any (`LBS.isSuffixOf` toLazyByteString (encodeIRI i)) genDelims
  let computedPrefix = case etd ^. etdIdL of
        Nothing -> False
        Just Reset -> False
        Just _ ->
          let termCanBePrefix = case term of
                KeyTerm _ -> True
                KeyReserved _ -> True
                KeyRelativeIRI _ -> True
                _ -> False
           in termCanBePrefix && simpleTerm && iriCanBePrefix
  prefix <-
    withMaybe computedPrefix (etd ^. etdPrefixL) $ \p -> do
      throwIfV1_0 InvalidTermDefinition
      case term of
        KeyIRI _ -> throwError InvalidTermDefinition
        KeyCompact _ -> throwError InvalidTermDefinition
        _ -> pure ()
      when p $
        case iriMapping of
          Just (Left _keyword) -> throwError InvalidTermDefinition
          _ -> pure ()
      pure p
  let newDefinition =
        ActiveTermDefinition
          { atdIRI = iriMapping,
            atdPrefix = prefix,
            atdProtected = protected,
            atdReverse = reverseMapping,
            atdLocalContext = localContext,
            atdContainerMapping = containerMapping,
            atdDirection = directionMapping,
            atdIndexMapping = indexMapping,
            atdLanguageMapping = languageMapping,
            atdNestMapping = nest,
            atdTypeMapping = typeMapping
          }
  mPreviousDefinition <- use (resultL . termDefsL . to (Map.lookup term))
  winningDefinition <-
    case mPreviousDefinition of
      Nothing -> pure newDefinition
      Just previousDefinition ->
        if not overrideProt && previousDefinition ^. protectedL
          then do
            let newDefWithOldProtected = newDefinition & protectedL .~ (previousDefinition ^. protectedL)
            when (previousDefinition /= newDefWithOldProtected) $ do
              throwError ProtectedTermRedefinition
            pure previousDefinition
          else pure newDefinition
  modify (resultL . termDefsL %~ Map.insert term winningDefinition)

expandIRI' ::
  forall m.
  (Monad m) =>
  -- base IRI
  Maybe IRI ->
  -- vocab IRI
  Maybe IRI ->
  (TermDefKey -> m (Maybe ActiveTermDefinition)) ->
  -- Default
  m (Maybe (Either Keyword IRI)) ->
  Either Keyword TermDefKey ->
  m (Maybe (Either Keyword IRI))
expandIRI' mBaseIRI mVocabIRI fetchTermDef def toIRI =
  case toIRI of
    Left k -> pure . Just $ Left k
    Right key -> expandTermDefKey key
  where
    expandTermDefKey termDefKey = do
      mIRIMapping <- (view iriL =<<) <$> fetchTermDef termDefKey
      case mIRIMapping of
        Just (Left k) -> pure . Just $ Left k
        Just (Right iri) | isJust mVocabIRI -> pure . Just $ Right iri
        _ ->
          case termDefKey of
            KeyIRI iri -> pure . Just $ Right iri
            KeyCompact (CompactIRI prefix suffix) -> do
              let prefixKey = KeyTerm prefix
              mPrefixDef <- fetchTermDef prefixKey
              case mPrefixDef of
                Just prefixDef | view prefixL prefixDef ->
                  case view iriL prefixDef of
                    Just (Right iri) -> pure . Just . Right $ appendToIRI iri suffix
                    _ -> expandWithVocabOrBaseIRI $ termAppendText prefix suffix
                _ -> expandWithVocabOrBaseIRI $ termAppendText prefix suffix
            KeyTerm t -> expandWithVocabOrBaseIRI t
            KeyRelativeIRI t -> expandWithVocabOrBaseIRI t
            KeyReserved _ -> pure Nothing

    expandWithVocabOrBaseIRI :: Term -> m (Maybe (Either Keyword IRI))
    expandWithVocabOrBaseIRI t = do
      case (mVocabIRI, mBaseIRI) of
        (Just vocabIRI, _) -> pure . Just . Right $ appendToIRI vocabIRI (termToText t)
        (_, Just currentBaseIRI) -> pure . Just . Right $ appendToIRI currentBaseIRI (termToText t)
        _ ->
          -- NOTE [Unexpandable String]
          --
          -- The final step actually says return "value" as is, but in this case
          -- it is definitely not an IRI or a keyword, so not sure what should
          -- actually be done here.
          def

expandIRIWithActiveContext :: Bool -> Bool -> ActiveContext -> Either Keyword TermDefKey -> Maybe (Either Keyword IRI)
expandIRIWithActiveContext isDocRelative vocabEnabled activeCtx toIRI =
  runIdentity $
    expandIRI'
      (guard isDocRelative *> acCurrentBaseIRI activeCtx)
      (guard vocabEnabled *> acVocab activeCtx)
      (pure . flip Map.lookup (acTermDefs activeCtx))
      (pure Nothing) -- FIXME: See note [Unexpandable String]
      toIRI

expandIRIInActiveContext :: Bool -> Bool -> Maybe ContextDefinition -> Map TermDefKey Bool -> Either Keyword TermDefKey -> ContextProcessing ActiveContext (Maybe (Either Keyword IRI))
expandIRIInActiveContext isDocRelative vocabEnabled mLocalCtx defined toIRI = do
  mActiveVocabMapping <- use (resultL . vocabL)
  mCurrentBaseIRI <- use (resultL . currentBaseIriL)
  case toIRI of
    Left k -> pure . Just $ Left k
    Right key -> do
      let fetchTermDef termDefKey = do
            computeActiveContextIfRequired termDefKey
            termDefFromActiveContext termDefKey
      expandIRI'
        (guard isDocRelative *> mCurrentBaseIRI)
        (guard vocabEnabled *> mActiveVocabMapping)
        fetchTermDef
        (throwError InvalidIRIMapping)
        (Right key)
  where
    termDefFromActiveContext :: TermDefKey -> ContextProcessing ActiveContext (Maybe ActiveTermDefinition)
    termDefFromActiveContext termDefKey =
      Map.lookup termDefKey <$> use (resultL . termDefsL)

    computeActiveContextIfRequired :: TermDefKey -> ContextProcessing ActiveContext ()
    computeActiveContextIfRequired termDefKey =
      case mLocalCtx of
        Nothing -> pure ()
        Just localCtx -> do
          let mTermDef = Map.lookup termDefKey $ cdTermDefinitions localCtx
              isDefined = Map.findWithDefault False termDefKey defined
          case mTermDef of
            Nothing -> pure ()
            Just termDef ->
              unless isDefined $ do
                -- TODO: Figure out if protected and override protected have to be
                -- hardcoded here.
                createTermDefinition localCtx [] True False defined termDefKey termDef

-- * Helpers

(.=) :: MonadState s m => ASetter s s a b -> b -> m ()
l .= b = modify (l .~ b)
{-# INLINE (.=) #-}

(?=) :: MonadState s m => ASetter s s a (Maybe b) -> b -> m ()
l ?= b = modify (l ?~ b)
{-# INLINE (?=) #-}

(<>=) :: (MonadState s m, Monoid a) => ASetter s s a a -> a -> m ()
l <>= a = modify (l <>~ a)
{-# INLINE (<>=) #-}

use :: MonadState s m => Getting a s a -> m a
use l = gets (view l)
{-# INLINE use #-}

infix 4 .=, ?=, <>=

withMaybe :: Applicative f => b -> Maybe a -> (a -> f b) -> f b
withMaybe b Nothing _ = pure b
withMaybe _ (Just a) f = f a

withJust :: Applicative f => Maybe a -> (a -> f ()) -> f ()
withJust Nothing _ = pure ()
withJust (Just a) f = f a

withJust' :: Applicative f => Maybe a -> (a -> f (Maybe b)) -> f (Maybe b)
withJust' Nothing _ = pure Nothing
withJust' (Just a) f = f a

withJustOrError :: MonadError e m => e -> Maybe a -> (a -> m b) -> m b
withJustOrError exc Nothing _ = throwError exc
withJustOrError _ (Just x) f = f x

-- | Monadic version of @when@, taking the condition in the monad
whenM :: Monad m => m Bool -> m () -> m ()
whenM mb thing = do
  b <- mb
  when b thing

-- | Monadic version of @unless@, taking the condition in the monad
unlessM :: Monad m => m Bool -> m () -> m ()
unlessM condM acc = do
  cond <- condM
  unless cond acc
