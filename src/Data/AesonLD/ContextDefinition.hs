{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Data.AesonLD.ContextDefinition where

import Control.Applicative (Alternative, (<|>))
import Control.Monad (guard, unless, when)
import Data.Aeson
import qualified Data.Aeson.KeyMap as KeyMap
import Data.Aeson.Types (typeMismatch)
import qualified Data.Aeson.Types as Aeson
import Data.AesonLD.Keyword
import Data.AesonLD.Term (Term, termFromText, termToText)
import qualified Data.Attoparsec.Text as Atto
import Data.BCP47
import qualified Data.ByteString.Builder as Builder
import qualified Data.ByteString.Lazy as LBS
import Data.Functor (($>))
import Data.Map (Map)
import Data.Maybe (fromMaybe, isJust)
import Data.RDF.Encoder.Common (encodeIRI)
import Data.RDF.Parser.Common (parseIRI)
import Data.RDF.Types (IRI (..))
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text
import qualified Data.Vector as V
import qualified Data.Vector as Vector
import Lens.Micro

newtype LocalContext = ContextList [Resettable SingleContext]
  deriving (Eq, Show)

contextListL :: Lens' LocalContext [Resettable SingleContext]
contextListL f (ContextList v) = fmap ContextList (f v)

instance FromJSON LocalContext where
  parseJSON (Array xs) = ContextList <$> mapM parseJSON (Vector.toList xs)
  parseJSON v = ContextList . (: []) <$> parseJSON v

data SingleContext
  = EmbeddedContext ContextDefinition
  | RemoteContext IRI
  deriving (Eq,Show)

_EmbeddedContext :: Traversal' SingleContext ContextDefinition
_EmbeddedContext f (EmbeddedContext cd) = EmbeddedContext <$> f cd
_EmbeddedContext _ ctx = pure ctx

_RemoteContext :: Traversal' SingleContext IRI
_RemoteContext f (RemoteContext iri) = RemoteContext <$> f iri
_RemoteContext _ ctx = pure ctx

instance FromJSON SingleContext where
  parseJSON (String t) = RemoteContext <$> iriFromText t
  parseJSON v = EmbeddedContext <$> parseJSON v

data ContextDefinition = ContextDefinition
  { cdBase :: Maybe (Resettable Base),
    cdDirection :: Maybe (Resettable Direction),
    cdImport :: Maybe IRIReference,
    cdLanguage :: Maybe (Resettable Language),
    cdPropagate :: Maybe Bool,
    cdProtected :: Maybe Bool,
    cdVersion :: Maybe Version,
    cdVocab :: Maybe (Resettable Vocab),
    cdTypeOverride :: Maybe TypeOverride,
    cdTermDefinitions :: Map TermDefKey (Resettable TermDefinition)
  }
  deriving (Show, Eq)

cdBaseL :: Lens' ContextDefinition (Maybe (Resettable Base))
cdBaseL f cd = fmap (\a' -> cd {cdBase = a'}) (f (cdBase cd))

cdDirectionL :: Lens' ContextDefinition (Maybe (Resettable Direction))
cdDirectionL f cd = fmap (\a' -> cd {cdDirection = a'}) (f (cdDirection cd))

cdImportL :: Lens' ContextDefinition (Maybe IRIReference)
cdImportL f cd = fmap (\a' -> cd {cdImport = a'}) (f (cdImport cd))

cdLanguageL :: Lens' ContextDefinition (Maybe (Resettable Language))
cdLanguageL f cd = fmap (\a' -> cd {cdLanguage = a'}) (f (cdLanguage cd))

cdPropagateL :: Lens' ContextDefinition (Maybe Bool)
cdPropagateL f cd = fmap (\a' -> cd {cdPropagate = a'}) (f (cdPropagate cd))

cdProtectedL :: Lens' ContextDefinition (Maybe Bool)
cdProtectedL f cd = fmap (\a' -> cd {cdProtected = a'}) (f (cdProtected cd))

cdVersionL :: Lens' ContextDefinition (Maybe Version)
cdVersionL f cd = fmap (\a' -> cd {cdVersion = a'}) (f (cdVersion cd))

cdVocabL :: Lens' ContextDefinition (Maybe (Resettable Vocab))
cdVocabL f cd = fmap (\a' -> cd {cdVocab = a'}) (f (cdVocab cd))

cdTypeOverrideL :: Lens' ContextDefinition (Maybe TypeOverride)
cdTypeOverrideL f cd = fmap (\a' -> cd {cdTypeOverride = a'}) (f (cdTypeOverride cd))

cdTermDefinitionsL :: Lens' ContextDefinition (Map TermDefKey (Resettable TermDefinition))
cdTermDefinitionsL f cd = fmap (\a' -> cd {cdTermDefinitions = a'}) (f (cdTermDefinitions cd))

{- ORMOLU_DISABLE -}
importContextDef :: ContextDefinition -> ContextDefinition -> ContextDefinition
importContextDef origContext importedContext =
  origContext
    & cdBaseL <|>~ importedContext ^. cdBaseL
    & cdDirectionL <|>~ importedContext ^. cdDirectionL
    & cdImportL <|>~ importedContext ^. cdImportL
    & cdLanguageL <|>~ importedContext ^. cdLanguageL
    & cdPropagateL <|>~ importedContext ^. cdPropagateL
    & cdProtectedL <|>~ importedContext ^. cdProtectedL
    & cdVersionL <|>~ importedContext ^. cdVersionL
    & cdVocabL <|>~ importedContext ^. cdVocabL
    & cdTypeOverrideL <|>~ importedContext ^. cdTypeOverrideL
    & cdTermDefinitionsL %~ (importedContext ^. cdTermDefinitionsL <>)
{- ORMOLU_ENABLE -}

instance FromJSON ContextDefinition where
  parseJSON = withObject "ContextDefinition" $ \o -> do
    let relevantKeywords = ["@base", "@direction", "@import", "@language", "@propagate", "@protected", "@version", "@vocab", "@type"]
    ContextDefinition
      <$> o .:! "@base"
      <*> o .:! "@direction"
      <*> o .:! "@import"
      <*> o .:! "@language"
      <*> o .:! "@propagate"
      <*> o .:! "@protected"
      <*> o .:! "@version"
      <*> o .:! "@vocab"
      <*> o .:! "@type"
      <*> parseJSON (Object (foldr KeyMap.delete o relevantKeywords))

data Resettable a
  = Set a
  | Reset
  deriving (Show, Eq)

_Set :: Traversal' (Resettable a) a
_Set f (Set a) = Set <$> f a
_Set _ Reset = pure Reset

_Reset :: Traversal' (Resettable a) ()
_Reset f Reset = Reset <$ f ()
_Reset _ s = pure s

resettableToMaybe :: Resettable a -> Maybe a
resettableToMaybe (Set a) = Just a
resettableToMaybe Reset = Nothing

instance FromJSON a => FromJSON (Resettable a) where
  parseJSON Null = pure Reset
  parseJSON v = Set <$> parseJSON v

iriFromJSON :: Value -> Aeson.Parser IRI
iriFromJSON = withText "IRI" iriFromText

iriFromText :: MonadFail f => Text -> f IRI
iriFromText x =
  case Atto.parseOnly (parseIRI <* Atto.endOfInput) x of
    Right iri -> pure iri
    Left err -> fail $ "Failed to parse IRI with: " <> err

appendToIRI :: IRI -> Text -> IRI
appendToIRI i suffix =
  let iriText = Text.decodeUtf8 . LBS.toStrict . Builder.toLazyByteString $ encodeIRI i
      newIriText = iriText <> suffix
   in fromMaybe (error $ "Impossible: Invalid IRI: " <> show newIriText) $ iriFromText newIriText

data IRIReference
  = RelativeIRI Term
  | AbsoluteIRI IRI
  deriving (Show, Eq)

instance FromJSON IRIReference where
  parseJSON = withText "IRIReference" $ \x ->
    AbsoluteIRI <$> iriFromText x
      <|> RelativeIRI <$> termFromText x

-- | Implements https://www.rfc-editor.org/rfc/rfc3986#section-5.2 as specified
-- in https://www.rfc-editor.org/rfc/rfc3987#section-6.5
resolveIRIReference :: IRIReference -> IRI -> IRI
resolveIRIReference (AbsoluteIRI iri) _ = iri
resolveIRIReference (RelativeIRI foo) base =
  let fixScheme i = i {iriScheme = iriScheme base}
   in case fixScheme <$> iriFromText ("fake://" <> termToText foo) of
        Just parsed
          | isJust (iriAuth parsed) ->
              parsed
                { iriPath = removeDotSegments (iriPath parsed)
                }
          | iriPath parsed == "" && isJust (iriQuery parsed) ->
              parsed {iriPath = iriPath base}
          | iriPath parsed == "" ->
              parsed {iriPath = iriPath base, iriQuery = iriQuery base}
          | "/" `Text.isPrefixOf` iriPath parsed ->
              parsed {iriPath = removeDotSegments (iriPath parsed)}
          | otherwise ->
              parsed {iriPath = removeDotSegments $ mergePaths base (iriPath parsed)}
        Nothing -> error "Impossible::resolveIRIReference"

mergePaths :: IRI -> Text -> Text
mergePaths base relPath
  | isJust (iriAuth base) && iriPath base == "" = "/" <> relPath
  | otherwise = Text.dropWhileEnd (/= '/') (iriPath base) <> relPath

{- ORMOLU_DISABLE -}

-- Copied from network-uri and adjusted from Text

--  Remove dot segments, but protect leading '/' character
removeDotSegments :: Text -> Text
removeDotSegments (Text.stripPrefix "/" -> Just ps) = "/" <> elimDots (Text.tail ps) []
removeDotSegments ps = elimDots ps []

--  Second arg accumulates segments processed so far in reverse order
elimDots :: Text -> [Text] -> Text
-- elimDots ps rs | traceVal "\nps " ps $ traceVal "rs " rs $ False = error ""
elimDots ""                                  [] = ""
elimDots ""                                  rs = Text.concat (reverse rs)
elimDots (Text.stripPrefix "./" -> Just ps)  rs = elimDots ps rs
elimDots "."                                 rs = elimDots "" rs
elimDots (Text.stripPrefix "../" -> Just ps) rs = elimDots ps (drop 1 rs)
elimDots ".."                                rs = elimDots "" (drop 1 rs)
elimDots ps rs = elimDots ps1 (r:rs)
    where
        (r,ps1) = nextSegment ps

--  Returns the next segment and the rest of the path from a path string.
--  Each segment ends with the next '/' or the end of string.
--
nextSegment :: Text -> (Text,Text)
nextSegment ps =
    case Text.break (=='/') ps of
        (r,Text.stripPrefix "/" -> Just ps1) -> (r <> "/",ps1)
        (r,_)       -> (r,"")
{- ORMOLU_ENABLE -}

data CompactIRI = CompactIRI Term Text
  deriving (Show, Eq, Ord)

instance FromJSON CompactIRI where
  parseJSON = withText "CompactIRI" compactIRIFromText

compactIRIFromText :: MonadFail m => Text -> m CompactIRI
compactIRIFromText x = do
  let (prefixStr, rest) = Text.breakOn ":" x
  prefix <- termFromText prefixStr

  unless (":" `Text.isPrefixOf` rest) $
    fail ("A compact IRI must contain a colon: " <> show x)

  -- 'rest' here should have ':' as a prefix, so we can say that it is not
  -- empty.
  let suffix = Text.tail rest
  when ("//" `Text.isPrefixOf` suffix) $
    fail ("A compact IRI suffix cannot begin with '//':" <> show x)

  pure $ CompactIRI prefix suffix

data Vocab
  = VocabCompact CompactIRI
  | VocabIRIReference IRIReference
  deriving (Show, Eq)

instance FromJSON Vocab where
  parseJSON v =
    VocabCompact <$> parseJSON v
      <|> VocabIRIReference <$> parseJSON v

newtype Base = Base IRIReference
  deriving (Show, Eq)

instance FromJSON Base where
  parseJSON = fmap Base . parseJSON

data Direction
  = LTR
  | RTL
  deriving (Show, Eq)

instance FromJSON Direction where
  parseJSON =
    withText "direction" $ \case
      "ltr" -> pure LTR
      "rtl" -> pure RTL
      invalid -> fail $ "Invalid direction: " <> show invalid

instance ToJSON Direction where
  toJSON = \case
    LTR -> String "ltr"
    RTL -> String "rtl"

newtype Language = Language BCP47
  deriving (Show, Eq)

instance FromJSON Language where
  parseJSON =
    fmap Language . parseJSON

instance ToJSON Language where
  toJSON (Language l) = toJSON l

data Version = Version1_1
  deriving (Show, Eq)

instance FromJSON Version where
  parseJSON =
    withScientific "version" $ \n ->
      if n == 1.1
        then pure Version1_1
        else fail $ "version can only be a 1.1, found: " <> show n

data TypeOverride
  = TypeContainerSet (Maybe Bool)
  | TypeProtected Bool
  deriving (Show, Eq)

instance FromJSON TypeOverride where
  parseJSON = withObject "TypeOverride" $ \o -> do
    prot <- o .:! "@protected"
    mContainer :: Maybe Text <- o .:! "@container"
    case mContainer of
      Nothing -> maybe (fail "niether @container, not @protected set for @type") (pure . TypeProtected) prot
      Just "@set" ->
        pure $ TypeContainerSet prot
      Just cont -> fail $ "@container can only be '@set', got: " <> show cont

data TermDefKey
  = KeyIRI IRI
  | KeyCompact CompactIRI
  | KeyTerm Term
  | KeyReserved Reserved
  | -- Here we don't use 'IRIReference' because it is not possible to
    -- differentiate between Term and Relative IRI when parsing. For a
    -- 'TermDefKey' to be a relative IRI, it needs to contain a '/'
    KeyRelativeIRI Term
  deriving (Show, Eq, Ord)

_KeyIRI :: Traversal' TermDefKey IRI
_KeyIRI f (KeyIRI x) = KeyIRI <$> f x
_KeyIRI _ td = pure td

_KeyCompact :: Traversal' TermDefKey CompactIRI
_KeyCompact f (KeyCompact x) = KeyCompact <$> f x
_KeyCompact _ td = pure td

_KeyTerm :: Traversal' TermDefKey Term
_KeyTerm f (KeyTerm x) = KeyTerm <$> f x
_KeyTerm _ td = pure td

_KeyReserved :: Traversal' TermDefKey Reserved
_KeyReserved f (KeyReserved x) = KeyReserved <$> f x
_KeyReserved _ td = pure td

_KeyRelativeIRI :: Traversal' TermDefKey Term
_KeyRelativeIRI f (KeyRelativeIRI x) = KeyRelativeIRI <$> f x
_KeyRelativeIRI _ td = pure td

instance FromJSON TermDefKey where
  parseJSON = withText "TermDefKey" termDefKeyFromText

termDefKeyFromText :: (Alternative f, MonadFail f) => Text -> f TermDefKey
termDefKeyFromText t =
  KeyIRI <$> iriFromText t
    <|> KeyReserved <$> parseReserved t
    <|> (guard ('\\' `Text.elem` t) >> KeyRelativeIRI <$> termFromText t)
    <|> KeyTerm <$> termFromText t

instance FromJSONKey TermDefKey where
  fromJSONKey = FromJSONKeyTextParser termDefKeyFromText

data TermDefinition
  = TDIRI IRI
  | TDCompact CompactIRI
  | TDTerm Term
  | TDKeyword Keyword
  | TDExpanded ExpandedTermDefinition
  deriving (Show, Eq)

_TDIRI :: Traversal' TermDefinition IRI
_TDIRI f (TDIRI x) = TDIRI <$> f x
_TDIRI _ td = pure td

_TDCompact :: Traversal' TermDefinition CompactIRI
_TDCompact f (TDCompact x) = TDCompact <$> f x
_TDCompact _ td = pure td

_TDTerm :: Traversal' TermDefinition Term
_TDTerm f (TDTerm x) = TDTerm <$> f x
_TDTerm _ td = pure td

_TDKeyword :: Traversal' TermDefinition Keyword
_TDKeyword f (TDKeyword x) = TDKeyword <$> f x
_TDKeyword _ td = pure td

_TDExpanded :: Traversal' TermDefinition ExpandedTermDefinition
_TDExpanded f (TDExpanded x) = TDExpanded <$> f x
_TDExpanded _ td = pure td

expandTermDefinitiion :: Resettable TermDefinition -> (ExpandedTermDefinition, Bool)
expandTermDefinitiion rtd =
  (\(f, b) -> (f emptyExpandedTermDefinition, b)) $ case rtd of
    Reset -> (etdIdL ?~ Reset, False)
    Set (TDIRI iri) -> (etdIdL ?~ Set (IdIRI iri), True)
    Set (TDCompact c) -> (etdIdL ?~ Set (IdCompact c), True)
    Set (TDTerm t) -> (etdIdL ?~ Set (IdTerm t), True)
    Set (TDKeyword k) -> (etdIdL ?~ Set (IdKeyword k), True)
    Set (TDExpanded e) -> (const e, False)

instance FromJSON TermDefinition where
  parseJSON =
    \case
      v@(String t) ->
        TDIRI <$> iriFromText t
          <|> TDCompact <$> parseJSON v
          <|> TDTerm <$> parseJSON v
          <|> TDKeyword <$> parseJSON v
      Object o -> TDExpanded <$> expandedTermDefinitionFromObject o
      v -> typeMismatch "String, Object or Null" v

data ExpandedTermDefinition = ExpandedTermDefinition
  { etdId :: Maybe (Resettable TermId),
    etdReverse :: Maybe Reverse,
    etdType :: Maybe CompactTermType,
    etdLanguage :: Maybe Language,
    etdDirection :: Maybe Direction,
    etdIndex :: Maybe Index,
    etdContainer :: Maybe (Resettable Container),
    etdContext :: Maybe LocalContext,
    etdNest :: Maybe Nest,
    etdPrefix :: Maybe Bool,
    etdPropagate :: Maybe Bool,
    etdProtected :: Maybe Bool
  }
  deriving (Show, Eq)

etdIdL :: Lens' ExpandedTermDefinition (Maybe (Resettable TermId))
etdIdL f cd = fmap (\a' -> cd {etdId = a'}) (f (etdId cd))

etdReverseL :: Lens' ExpandedTermDefinition (Maybe Reverse)
etdReverseL f cd = fmap (\a' -> cd {etdReverse = a'}) (f (etdReverse cd))

etdTypeL :: Lens' ExpandedTermDefinition (Maybe CompactTermType)
etdTypeL f cd = fmap (\a' -> cd {etdType = a'}) (f (etdType cd))

etdLanguageL :: Lens' ExpandedTermDefinition (Maybe Language)
etdLanguageL f cd = fmap (\a' -> cd {etdLanguage = a'}) (f (etdLanguage cd))

etdDirectionL :: Lens' ExpandedTermDefinition (Maybe Direction)
etdDirectionL f cd = fmap (\a' -> cd {etdDirection = a'}) (f (etdDirection cd))

etdIndexL :: Lens' ExpandedTermDefinition (Maybe Index)
etdIndexL f cd = fmap (\a' -> cd {etdIndex = a'}) (f (etdIndex cd))

etdContainerL :: Lens' ExpandedTermDefinition (Maybe (Resettable Container))
etdContainerL f cd = fmap (\a' -> cd {etdContainer = a'}) (f (etdContainer cd))

etdContextL :: Lens' ExpandedTermDefinition (Maybe LocalContext)
etdContextL f cd = fmap (\a' -> cd {etdContext = a'}) (f (etdContext cd))

etdNestL :: Lens' ExpandedTermDefinition (Maybe Nest)
etdNestL f cd = fmap (\a' -> cd {etdNest = a'}) (f (etdNest cd))

etdPrefixL :: Lens' ExpandedTermDefinition (Maybe Bool)
etdPrefixL f cd = fmap (\a' -> cd {etdPrefix = a'}) (f (etdPrefix cd))

etdPropagateL :: Lens' ExpandedTermDefinition (Maybe Bool)
etdPropagateL f cd = fmap (\a' -> cd {etdPropagate = a'}) (f (etdPropagate cd))

etdProtectedL :: Lens' ExpandedTermDefinition (Maybe Bool)
etdProtectedL f cd = fmap (\a' -> cd {etdProtected = a'}) (f (etdProtected cd))

instance FromJSON ExpandedTermDefinition where
  parseJSON = withObject "ExpandedTermDefinition" expandedTermDefinitionFromObject

expandedTermDefinitionFromObject :: Object -> Aeson.Parser ExpandedTermDefinition
expandedTermDefinitionFromObject o =
  ExpandedTermDefinition
    <$> o .:! "@id"
    <*> o .:! "@reverse"
    <*> o .:! "@type"
    <*> o .:! "@language"
    <*> o .:! "@direction"
    <*> o .:! "@index"
    <*> o .:! "@container"
    <*> o .:! "@context"
    <*> o .:! "@nest"
    <*> o .:! "@prefix"
    <*> o .:! "@propagate"
    <*> o .:! "@protected"

emptyExpandedTermDefinition :: ExpandedTermDefinition
emptyExpandedTermDefinition =
  ExpandedTermDefinition
    { etdId = Nothing,
      etdReverse = Nothing,
      etdType = Nothing,
      etdLanguage = Nothing,
      etdDirection = Nothing,
      etdIndex = Nothing,
      etdContainer = Nothing,
      etdContext = Nothing,
      etdNest = Nothing,
      etdPrefix = Nothing,
      etdPropagate = Nothing,
      etdProtected = Nothing
    }

data TermId
  = IdIRI IRI
  | IdCompact CompactIRI
  | IdTerm Term
  | IdKeyword Keyword
  | IdReserved Reserved
  deriving (Show, Eq)

instance FromJSON TermId where
  parseJSON =
    withText "TermId" $ \t ->
      IdIRI <$> iriFromText t
        <|> IdCompact <$> compactIRIFromText t
        <|> IdTerm <$> termFromText t
        <|> parseKeywordOrReserved IdKeyword IdReserved t

data CompactTermType
  = CompactTypeIRI IRI
  | CompactTypeCompact CompactIRI
  | CompactTypeTerm Term
  | CompactTypeKeyword Keyword
  | CompactTypeReserved Reserved
  deriving (Show, Eq)

instance FromJSON CompactTermType where
  parseJSON =
    withText "CompactTermType" $ \t ->
      CompactTypeIRI <$> iriFromText t
        <|> CompactTypeCompact <$> compactIRIFromText t
        <|> CompactTypeTerm <$> termFromText t
        <|> CompactTypeKeyword <$> keywordTypeFromText t
        <|> CompactTypeReserved <$> parseReserved t

keywordTypeFromText :: MonadFail m => Text -> m Keyword
keywordTypeFromText t = do
  k <- parseKeyword t
  if k `elem` [KeywordJson, KeywordNone, KeywordId, KeywordVocab]
    then pure k
    else fail $ "Invalid type: " <> show k

data Reverse
  = ReverseIRI IRI
  | ReverseCompact CompactIRI
  | ReverseTerm Term
  | ReverseReserved Reserved
  deriving (Show, Eq)

instance FromJSON Reverse where
  parseJSON =
    withText "Reverse" $ \t ->
      ReverseIRI <$> iriFromText t
        <|> ReverseCompact <$> compactIRIFromText t
        <|> ReverseTerm <$> termFromText t
        <|> ReverseReserved <$> parseReserved t

data Index
  = IndexIRI IRI
  | IndexCompact CompactIRI
  | IndexTerm Term
  deriving (Show, Eq)

instance FromJSON Index where
  parseJSON v =
    IndexIRI <$> iriFromJSON v
      <|> IndexCompact <$> parseJSON v
      <|> IndexTerm <$> parseJSON v

data GraphContainer = GraphContainer
  { gcId :: Bool,
    gcIndex :: Bool,
    gcSet :: Bool
  }
  deriving (Show, Eq, Ord)

graphContainerFromList :: MonadFail f => [Text] -> f GraphContainer
graphContainerFromList xs =
  let id_ = "@id" `elem` xs
      index = "@index" `elem` xs
      set_ = "@set" `elem` xs
      graphExists = "@graph" `elem` xs
      nothingElseExists = all (\x -> x == "@id" || x == "@index" || x == "@set" || x == "@graph") xs
   in if graphExists && nothingElseExists
        then pure $ GraphContainer id_ index set_
        else fail ""

data SetContainer
  = SetContainerId
  | SetContainerIndex
  | SetContainerType
  | SetContainerLanguage
  deriving (Show, Eq, Ord)

setContainerFromList :: forall f. MonadFail f => [Text] -> f SetContainer
setContainerFromList = \case
  ["@set", other] -> parseOther other
  [other, "@set"] -> parseOther other
  xs -> fail $ "Invalid set container: " <> show xs
  where
    parseOther :: Text -> f SetContainer
    parseOther = \case
      "@id" -> pure SetContainerId
      "@index" -> pure SetContainerIndex
      "@type" -> pure SetContainerType
      "@language" -> pure SetContainerLanguage
      t -> fail $ "Invalid set container: " <> show ["@set", t]

data Container
  = ContainerId
  | ContainerLanguage
  | ContainerIndex
  | ContainerType
  | ContainerList
  | ContainerGraph GraphContainer
  | ContainerSet SetContainer
  deriving (Show, Eq, Ord)

_ContainerId :: Traversal' Container ()
_ContainerId f ContainerId = f () $> ContainerId
_ContainerId _ td = pure td

_ContainerLanguage :: Traversal' Container ()
_ContainerLanguage f ContainerLanguage = f () $> ContainerLanguage
_ContainerLanguage _ td = pure td

_ContainerIndex :: Traversal' Container ()
_ContainerIndex f ContainerIndex = f () $> ContainerIndex
_ContainerIndex _ td = pure td

_ContainerType :: Traversal' Container ()
_ContainerType f ContainerType = f () $> ContainerType
_ContainerType _ td = pure td

_ContainerList :: Traversal' Container ()
_ContainerList f ContainerList = f () $> ContainerList
_ContainerList _ td = pure td

_ContainerGraph :: Traversal' Container GraphContainer
_ContainerGraph f (ContainerGraph x) = ContainerGraph <$> f x
_ContainerGraph _ td = pure td

_ContainerSet :: Traversal' Container SetContainer
_ContainerSet f (ContainerSet x) = ContainerSet <$> f x
_ContainerSet _ td = pure td

containerHasType :: Container -> Bool
containerHasType = \case
  ContainerType -> True
  ContainerSet SetContainerType -> True
  _ -> False

contianerHasId :: Container -> Bool
contianerHasId = \case
  ContainerId -> True
  ContainerSet SetContainerId -> True
  ContainerGraph g -> gcId g
  _ -> False

containerHasIndex :: Container -> Bool
containerHasIndex = \case
  ContainerIndex -> True
  ContainerSet SetContainerIndex -> True
  ContainerGraph g -> gcIndex g
  _ -> False

instance FromJSON Container where
  parseJSON = \case
    String t -> parseJSON (Array . V.singleton $ String t)
    Array vs -> do
      texts <- parseJSON @[Text] (Array vs)
      case texts of
        ["@id"] -> pure ContainerId
        ["@language"] -> pure ContainerLanguage
        ["@index"] -> pure ContainerIndex
        ["@type"] -> pure ContainerType
        ["@list"] -> pure ContainerList
        xs | "@graph" `elem` xs -> ContainerGraph <$> graphContainerFromList xs
        xs | "@set" `elem` xs -> ContainerSet <$> setContainerFromList xs
        xs -> fail $ "Invalid container: " <> show xs
    v -> Aeson.typeMismatch "String, Array or Null" v

data Nest
  = Nest
  | NestTerm Term
  deriving (Show, Eq)

instance FromJSON Nest where
  parseJSON = withText "Nest" $ \case
    "@nest" -> pure Nest
    t -> NestTerm <$> termFromText t

(<|>~) :: Alternative f => ASetter s t (f a) (f a) -> f a -> s -> t
setter <|>~ alt = setter %~ (<|> alt)

infix 4 <|>~
