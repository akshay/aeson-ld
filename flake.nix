{
  description = "Dev Setup";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.rdf = {
    url = "github:akshaymankar/rdf/fix-parser";
    flake = false;
  };

  outputs = inputs@{nixpkgs, flake-utils, ...}:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        # Avoids unnecessary recompiles
        filteredSource = pkgs.lib.cleanSourceWith {
          src = ./.;
          filter = path: type:
            let baseName = baseNameOf (toString path);
            in pkgs.lib.cleanSourceFilter path type && !(
              baseName == "flake.nix" ||
              baseName == "flake.lock" ||
              baseName == "dist-newstyle" ||
              builtins.match "^cabal\.project\..*$" baseName != null ||
              baseName == "hls.sh" ||
              baseName == ".envrc" ||
              baseName == "hie.yaml" ||
              baseName == ".hlint.yaml" ||
              baseName == ".hspec" ||
              baseName == "ci"
            );
        };
        ghcOverrides = hself: hsuper: rec {
          aeson-ld = hself.callCabal2nix "aeson-ld" filteredSource {};
          country = pkgs.haskell.lib.markUnbroken hsuper.country;
          primitive-checked = pkgs.haskell.lib.markUnbroken hsuper.primitive-checked;
          # rdf = pkgs.haskell.lib.markUnbroken (pkgs.haskell.lib.doJailbreak hsuper.rdf);
          rdf = hsuper.callCabal2nix "rdf" inputs.rdf {};
        };
        ghc924Pkgs = pkgs.haskell.packages.ghc924.override {
          overrides = ghcOverrides;
        };
        ghc902Pkgs = pkgs.haskell.packages.ghc902.override {
          overrides = ghcOverrides;
        };
        ghc8107Pkgs = pkgs.haskell.packages.ghc8107.override {
          overrides = ghcOverrides;
        };
        ghc884Pkgs = pkgs.haskell.packages.ghc884.override {
          overrides = ghcOverrides;
        };
      in rec {
        packages = rec {
          dev-env = ghc924Pkgs.shellFor {
            packages = p: [p.aeson-ld];
            buildInputs = [
              pkgs.haskellPackages.cabal-install
              pkgs.haskell-language-server
              pkgs.haskellPackages.implicit-hie
              pkgs.cabal2nix
              pkgs.ormolu

              # For cabal
              pkgs.pkgconfig
              pkgs.binutils

              # For CI
              pkgs.jq
              pkgs.dhall
              pkgs.dhall-json
              pkgs.fly
            ];
          };
          aeson-ld-ghc924 = ghc924Pkgs.aeson-ld;
          aeson-ld-ghc902 = ghc902Pkgs.aeson-ld;
          aeson-ld-ghc8107 = ghc8107Pkgs.aeson-ld;
          ormolu = ghc924Pkgs.ormolu_0_5_0_0;
        };
        defaultPackage = packages.dev-env;
    });
}
